﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CommunityWorkshop.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SessionDetailsView : ContentPage
	{
        private readonly Session _session;
		public SessionDetailsView (Session session)
		{
            _session = session;
			InitializeComponent ();
            Title = session.Name;
            BindingContext = session;
		}
	}
}