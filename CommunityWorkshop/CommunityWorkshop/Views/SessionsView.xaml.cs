﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CommunityWorkshop.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SessionsView : ContentPage
    {
        public SessionsView()
        {
            InitializeComponent();
            Sessions = new ObservableCollection<Session>();
        }

        public ObservableCollection<Session> Sessions { get; set; }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if(Sessions.Count == 0)
            {
                var sessions = SessionsDataSource.GetSessions();
                foreach (var session in sessions)
                {
                    Sessions.Add(session);
                }
            }
        }

        private void SessionsList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if(SessionsList.SelectedItem == null)
            {
                return;
            }

            SessionsList.SelectedItem = null;
        }

        private void FavoriteGesture_Tapped(object sender, EventArgs e)
        {
            var tappedEventArg = e as TappedEventArgs;
            if(tappedEventArg != null)
            {
                var session = tappedEventArg.Parameter as Session;
                if(session != null)
                {
                    session.IsFavorite = !session.IsFavorite;
                }
            }
        }

        private void DetailsGesture_Tapped(object sender, EventArgs e)
        {
            var tappedEventArg = e as TappedEventArgs;
            if(tappedEventArg != null)
            {
                var session = tappedEventArg.Parameter as Session;
                if(session != null)
                {
                    Navigation.PushAsync(new Views.SessionDetailsView(session), true);
                }
            }
        }
    }
}